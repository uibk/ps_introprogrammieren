#include <stdlib.h>
#include <stdio.h>
#include "a.h"
#include "b.h"

struct A
{
    unsigned int m_value;
};

struct B
{
    float m_data;
    struct A *m_a;
};

struct C
{
    struct B *m_b;
};

typedef struct person
{
    char *name;
    char *adress;
    int id;
} Person;

unsigned int strlenght(const char *text);
unsigned int tick(void);
void swap_ptr(int **a_ptr, int **b_ptr);
Person *createPerson(char *username, char *adress, int id);
void changePerson(Person *person, char *username, char *adress, int id);

int main(int argc, char const *argv[])
{
    // 1. Aufgabe
    // int arr[16] = {[4] = 42};

    // 2. Aufgabe
    // char str = "Hello";
    // printf("%u\n", strlenght(str));

    //  3. Aufgabe
    // the register keyword tells the compiler that the variable will be heavily used and may need much space

    // 4. Aufgabe
    /* int x = 0;
     for(int i = 0; i < 10; i++)
     {
         x = tick();
     }
    printf("%u\n", x);*/
    // 5. Aufgabe
    // Complicated solution: printf("%d\n", ((16 | 4) >> 2) ^ 1);
    // Simple Solution: printf("%d\n", 1 << 2);

    // 6. Aufgabe
    // dereferencing();

    // 7. Aufgabe
    // int a = 0;
    // int b = 1;

    // int *a_ptr = &a;
    // int *b_ptr = &b;

    // printf("%d %d\n", *a_ptr, *b_ptr);
    // swap_ptr(&a_ptr, &b_ptr);
    // printf("%d %d\n", *a_ptr, *b_ptr);

    // 8. Aufgabe
    // a++;
    // b++;

    // 9. Aufgabe
    // struct C x = x.m_b->m_a->m_value;

    // 10. Aufgabe
    /*
    Der Unterschied zwischen Unions und Structs sind, dass bei Structs alle enthaltenen
    Datenfelder Speicher benötigen, wobei Unions nur vom größten Element den Speicher benötigt (+ Alignment!) und dementsprechend initiliasieren muss
    */

    return 0;
}


// 2. Aufgabe
unsigned int strlenght(const char *text)
{
    unsigned int length = 0;
    while (*text != '\0')
    {
        length++;
        text++;
    }

    return length;
}

// 4. Aufgabe
unsigned int tick(void)
{
    static unsigned int count = 0;
    return ++count;
}

// 6. Aufgabe
void dereferencing()
{
    int **arr_dynamic = malloc(sizeof(int *) * 4);
    for (int i = 0; i < 4; i++)
    {
        arr_dynamic[i] = malloc(sizeof(int) * 4);
    }
    arr_dynamic[2][3] = 5555;
    int b = *(*(arr_dynamic + 2) + 3);
    printf("%d\n", b);
}

// 7. Aufgabe
void swap_ptr(int **a_ptr, int **b_ptr)
{
    int *temp = *a_ptr;
    *a_ptr = *b_ptr;
    *b_ptr = temp;
}
