#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

typedef struct point
{
    double x;
    double y;
    char *description;
} Point;

bool legal_input(double x, double y, char *description)
{
    bool legal = true;
    if ((x < 0 || y < 0 || strlen(description) <= 0) || *description == '\n')
    {
        legal = false;
    }
    return legal;
}

Point create_point(double x, double y, char *description)
{
    Point point;
    point.x = x;
    point.y = y;
    point.description = description;
    return point;
}

int sort_description(char *description1, char *description2)
{
    for (int i = 0; i < strlen(description1); i++)
    {
        if (description1[i] > description2[i])
        {
            return 1;
        }
    }
    return 0;
}

int null_point(Point point, Point point2)
{
    if (point.x > point2.x)
    {
        if (point.y > point2.y)
        {
            return 1;
        }
    }
    return 0;
}

void sort_pointlist(Point *pointList)
{
    for (int i = 0; pointList[i].description != NULL; i++)
    {
        // if (null_point(*(pointList)) > null_point(*(pointList+1)))
        // {
        //     Point tempPoint = *(pointList+ 1);
        //     pointList = pointList + 1;
        //     pointList = &tempPoint;
        // }
        for (int j = 0; j < i; j++)
        {
            if (null_point(pointList[j], pointList[j + 1]) == 1)
            {
                Point tempPoint = pointList[j];
                pointList[j] = pointList[j + 1];
                pointList[j] = tempPoint;
            }
        }
        // else if (null_point(*(pointList)) == null_point(*(pointList + 1)))
        // {
        //     if (sort_description((pointList)->description, (pointList + 1)->description) == 1)
        //     {
        //         Point tempPoint = *(pointList + 1);
        //         *(pointList + 1) = *(pointList);
        //         *(pointList) = tempPoint;
        //     }
        // }
    }
}

void print_pointlist(Point *pointList)
{
    for (int i = 0; pointList->description != NULL; i++)
    {
        printf("%f,%f:%s\n", pointList->x, pointList->y, pointList->description);
        pointList++;
    }
}

void scan_input(Point *pointList)
{
    bool validValues = false;
    int i = 0;
    do
    {
        double x, y = 0.00;
        char *description = malloc(sizeof(char) * 50);
        validValues = false;
        scanf("%lf,%lf:%s", &x, &y, description);
        if (legal_input(x, y, description) && (x != '\n' || y != '\n' || *description != '\n'))
        {
            validValues = true;
            Point point = create_point(x, y, description);
            pointList[i] = point;
        }
        fflush(stdin);
        i++;
    } while (validValues);
}

int main(int argc, char const *argv[])
{
    Point *pointList = malloc(sizeof(Point) * 10);
    scan_input(pointList);
    print_pointlist(pointList);
    sort_pointlist(pointList);
    print_pointlist(pointList);
    return 0;
}
