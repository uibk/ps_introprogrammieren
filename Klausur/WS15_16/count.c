#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

bool legal_string(char *string)
{
    bool legal = true;
    int stringLength = strlen(string);
    for (int i = 0; i < stringLength; i++)
    {
        if (!isalpha(string[i]))
        {
            legal = false;
        }
    }
    return legal;
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("missing input!");
        exit(0);
    }

    int array[26] = {0};
    for (int i = 1; i < argc; i++)
    {
        if (legal_string(argv[i]) && strlen(argv[i]) <= 10)
        {
            for (int j = 0; j < strlen(argv[i]); j++)
            {
                array[tolower(argv[i][j]) - 'a']++;
            }
        }
    }

    for (int i = 0; i < 26; i++)
    {
        if (array[i] != 0)
        {
            printf("%c: %d\n", toupper(i + 'a'), array[i]);
        }
    }

    return 0;
}
