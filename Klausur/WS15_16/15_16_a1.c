#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool legal_string(char* string) {
    bool test = true;
    for (int i = 0; i < strlen(string); i++) {
        if (!isalpha(*(string + i))) {
            test = false;
        }
    }
    return test;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("missing input\n");
        return EXIT_FAILURE;
    }

    int array[26] = {0};

    for (int i = 1; i < argc; i++) {
        if (legal_string(argv[i])) {
            for (int j = 0; j < strlen(argv[i]); j++) {
                array[tolower(argv[i][j]) - 'a']++;
            }
        }
    }

    for (int i = 0; i < 26; i++) {
        if (array[i] != 0) {
            printf("%c: %d\n", i + 'a', array[i]);
        }
    }

    return EXIT_SUCCESS;
}
