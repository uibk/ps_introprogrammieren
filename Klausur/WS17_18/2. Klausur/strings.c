#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

char *clear(const char *input)
{
    int i = 0;
    char *output = malloc(sizeof(char) * 10);
    while (input[i] != '\0')
    {
        output[i] = '-';
        i++;
    }
    return output;
}
char *mirror(const char *input)
{
    int size = strlen(input);
    char *output = malloc(sizeof(char) * (size * 2));
    int i = 0;
    for (; i < size; i++)
    {
        output[i] = input[i];
    }
    int j = i;
    int x = 0;
    while (input[x] != '\0')
    {
        output[j] = input[size - 1 - x];
        j++;
        x++;
    }

    return output;
}
char *reverse(const char *input)
{
    int size = strlen(input);
    char *output = malloc(sizeof(char) * (size * 2));
    for (int i = 0; i < size; i++)
    {
        output[i] = input[size - 1 - i];
    }

    return output;
}
char *up(const char *input)
{
    int i = 0;
    int size = strlen(input);
    char *output = malloc(sizeof(char) * size);
    while (input[i] != '\0')
    {
        if (!isupper(input[i]))
        {
            output[i] = toupper(input[i]);
        }
        else
        {
            printf("Ein Fehler ist aufgetreten: Grossbuchstabe im Manipulationsstring!\n");
            exit(0);
        }
        i++;
    }

    return output;
}

char *commandoLine(char *input, const char *method)
{
    int i = 0;
    char *output = malloc(sizeof(char) * 100);
    output = input;
    while (method[i] != '\0')
    {
        if (method[i] == 'c')
        {
            output = clear(output);
            i++;
        }
        if (method[i] == 'm')
        {
            output = mirror(output);
            i++;
        }
        if (method[i] == 'r')
        {
            output = reverse(output);
            i++;
        }
        if (method[i] == 'u')
        {
            output = up(output);
            i++;
        }
    }
    return output;
}

int main(int argc, char *argv[])
{
    printf("%s\n", commandoLine(argv[1], argv[2]));
    return 0;
}
