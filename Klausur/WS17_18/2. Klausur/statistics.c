#include <stdlib.h>
#include <stdio.h>

typedef struct stats
{
    int maximum;
    int minimum;
    double average;
} Stats;

int minValue(int *array, int size)
{
    int temp = 0;
    for (int i = 0; i < size; i++)
    {
        temp = array[i];
        if (temp > array[i + 1])
        {
            temp = array[i + 1];
        }
    }
    return temp;
}
int maxValue(int *array, int size)
{
    int temp = 0;
    for (int i = 0; i < size; i++)
    {
        temp = array[i];
        if (temp < array[i + 1])
        {
            temp = array[i + 1];
        }
    }
    return temp;
}
int avgValue(int *array, int size)
{
    int sum = 0;
    for (int i = 0; i < size; i++)
    {
        sum += array[i];
    }
    return (sum / size);
}

void printArray(Stats *array, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("Min: %d, ", array[i].minimum);
        printf("Max: %d, ", array[i].maximum);
        printf("Avg: %f\n", array[i].average);
    }
}
void printStat(Stats stat, int size)
{
    printf("Min: %d, ", stat.minimum);
    printf("Max: %d, ", stat.maximum);
    printf("Avg: %f\n", stat.average);
}

Stats stat(int *array, int size)
{
    int max = maxValue(array, size);
    int min = minValue(array, size);
    int avg = avgValue(array, size);
    Stats stat = {max, min, avg};
    printf("stat:\n");
    printStat(stat, size);
    return stat;
}

Stats *runningstat(int *array, int size)
{
    Stats *statsArray = malloc(sizeof(Stats) * size);

    for (int i = 0; i < size; i++)
    {
        statsArray[i] = stat(array, i + 1);
    }
    printf("runningstat:\n");
    printArray(statsArray, size);
    return statsArray;
}

int *arrayInit(unsigned int size)
{
    int *intArray = malloc(sizeof(int) * size);
    printf("Array content:\n");
    for (int i = 0; i < size; i++)
    {
        intArray[i] = rand() % 65537;
        printf("%d ", intArray[i]);
    }
    printf("\n");

    return intArray;
}

int main(int argc, char const *argv[])
{
    int size = 0;
    printf("Please tell me the size:\n");
    scanf("%d", &size);
    int *array = arrayInit(size);
    Stats stat1 = stat(array, size);
    Stats *statsArray = runningstat(array, size);
    return 0;
}
