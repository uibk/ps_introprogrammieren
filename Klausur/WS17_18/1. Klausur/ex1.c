#include <stdio.h>
#include <stdlib.h>
#include <limits.h> // INT_MIN und INT_MAX enthalten den kleinsten und größten möglichen Integerwert

// Gibt das Array arr formatiert auf dem Bildschirm aus
void printarr(int arr[], int length)
{
  printf("[");
  for (int i = 0; i < length; i++)
  {
    printf("%d", arr[i]);
    if (i < length - 1)
    {
      printf(", ");
    }
  }
  printf("]\n");
}

// Gibt die kleinste gerade Zahl des Arrays arr zurück
int evenmin(int arr[], int length)
{
  int temp = 0;
  for (int i = 0; i < length; i++)
  {
    if (arr[i] % 2 == 0)
    {
      temp = arr[i];
      if (arr[i] < arr[i + 1])
      {
        temp = arr[i];
      }
    }
  }

  return temp;
}

// Gibt den Durchschnittswert aller im Array arr enthaltenen Werte
// zurück
double mean(int arr[], int length)
{
  double sum = 0;
  for (int i = 0; i < length; i++)
  {
    sum += arr[i];
  }
  return sum / length;
}

// Gibt die umgedreht kumulative Summe des Arrays arr im Array result zurück
void reversecumsum(int arr[], int result[], int length)
{
  int sum = 0;
  int i = length - 1;
  for (; i >= 0; i--)
  {
    sum += arr[i];
    result[length - i - 1] = sum;
  }
}

int main(void)
{
  // Definieren Sie ihr Array (arr) und füllen Sie es mit den Werten
  // {23, 42, 3, -11, 15, 60, 47, 12, 1, 3}
  int arr[10] = {23, 42, 3, -11, 15, 60, 47, 12, 1, 3};
  // Definieren Sie eine Variable um die größe der Arrays zu speichern (arrsize)
  int arrsize = (sizeof(arr) / sizeof(int));
  // Definieren Sie das Array welches die Rückgabewerte von reversecumsum
  // auffängt (resarr)
  int resarr[10] = {0};
  printf("Inhalt von arr = ");
  printarr(arr, arrsize);

  printf("Gerades Minimum von arr = %d\n", evenmin(arr, arrsize));
  printf("Durchschnitt von arr = %f\n", mean(arr, arrsize));

  reversecumsum(arr, resarr, arrsize);
  printf("Umgedrehte kumulative Summe von arr = \n");
  printarr(resarr, arrsize);

  return EXIT_SUCCESS;
}

// Erwartete Bildschirmausgabe bei korrekter Implementierung:
// ---------------------------------------------------------
// Inhalt von arr = [23, 42, 3, -11, 15, 60, 47, 12, 1, 3]
// Gerades Minimum von arr = 12
// Durchschnitt von arr = 19.500000
// Umgedrehte kumulative Summe von arr =
// [3, 4, 16, 63, 123, 138, 127, 130, 172, 195]
