#include <stdio.h>
#include <stdlib.h>

struct Array
{
  // Definition ihrer Struktur
  int size;
  int *elementList;
};

// Erzeugt ein dynamisches Array
struct Array init(int size)
{
  int *elements = calloc(size, sizeof(int));

  if (elements == NULL)
  {
    size = 0;
  }

  struct Array array = {size, elements};

  return array;
}

// Setzt ein Element des dynamischen Arrays auf den Wert val und
// vergrößert das Array falls nötig
void set(struct Array *arr, int index, int val)
{
  if (index >= 0)
  {
    if (index > (arr->size - 1))
    {
      printf("%d\n\n", arr->size);
      arr->elementList = (int *)realloc(arr->elementList, sizeof(int) * (index + 1));
      for (int i = arr->size - 1; i <= index; i++)
      {
        arr->elementList[i] = 0;
      }
      arr->size = index + 1;
      arr->elementList[index] = val;
    }
    else
    {
      arr->elementList[index] = val;
    }
  }
}

// Gibt den Inhalt des dynamischen Arrays an einem bestimmten Index aus
int get(struct Array *arr, int index)
{
  if (index <= arr->size - 1 && index >= 0)
  {
    return arr->elementList[index];
  }
  return 0;
}

// Gibt den reservierten Speicher eines dynamischen Arrays frei
void freearr(struct Array *arr)
{
  if (arr->elementList != NULL)
  {
    free(arr->elementList);
    arr->elementList = NULL;
    arr->size = 0;
  }
}

// Gibt den Inhalt eines dynamischen Arrays formatiert auf dem Bildschirm aus
void print(struct Array *arr)
{
  if (arr->elementList != NULL)
  {
    printf("[");
    for (int i = 0; i < arr->size; i++)
    {
      printf("%d", arr->elementList[i]);
      if (i < arr->size - 1)
      {
        printf(", ");
      }
    }
    printf("]\n");
  }
}

int main(void)
{
  struct Array arr = init(10);
  print(&arr);
  set(&arr, 1, 42);
  print(&arr);
  set(&arr, -23, 23);
  print(&arr);
  set(&arr, 23, 23);
  print(&arr);
  printf("arr[1] = %d\n", get(&arr, 1));
  printf("arr[23] = %d\n", get(&arr, 23));
  printf("arr[24] = %d\n", get(&arr, 24));
  printf("arr[-10] = %d\n", get(&arr, -10));
  printf("arr[50] = %d\n", get(&arr, 50));
  freearr(&arr);
  freearr(&arr); // Soll keinen Fehler erzeugen
  print(&arr);
  return 0;
}

// Erwartete Ausgabe bei korrekter Implementierung
// -----------------------------------------------
// [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
// [0, 42, 0, 0, 0, 0, 0, 0, 0, 0]
// [0, 42, 0, 0, 0, 0, 0, 0, 0, 0]
// [0, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23]
// arr[1] = 42
// arr[23] = 23
// arr[24] = 0
// arr[-10] = 0
// arr[50] = 0
// []
