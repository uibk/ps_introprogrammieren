#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include "encrypt.h"

int encryptText(int key, int c, bool isLeft) {
    
    if (isLeft) {
        for (int i = key; i > 0; i--) {
            c -= 1;
            if(c < 97) {
                c = 122;
            }
         }
        } else {
             for (int i = 1; i <= key; i++) {
            c += 1;
            if(c >= 122) {
                c = 97;
            }
         }
        }
        return c;
}

int main(void)
{
    char c;
    int key = 3;
    bool isLeft = true;

    while ((c = getchar()) != '\n')
    {
        if(c == 32) {
            c = ' ';
        } else { 
        c = encryptText(key, c, isLeft);
        }
        putchar(c);
    }
    putchar('\n');
    return 0;
}

