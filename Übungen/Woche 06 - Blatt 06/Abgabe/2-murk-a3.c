#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include "calculation.h"
#include "binarynumbers.h"

void printBinaryNumbers(unsigned int n)
{
    for (int i = (sizeof(n) * CHAR_BIT) - 1; i >= 0; i--)
    {
        putchar('0' + ((n >> i) & 1));
    }
    printf("  =  %d\n", n);
}

unsigned int calculateOperation(
    char operation,
    int operand,
    unsigned int n)
{
    switch (operation)
    {
    case '=':
        n = operand;
        break;
    case '+':
        n += operand;
        break;
    case '-':
        n -= operand;
        break;
    case '/':
        n /= operand;
        break;
    case '*':
        n *= operand;
        break;
    case '|':
        n |= operand;
        break;
    case '&':
        n &= operand;
        break;
    case '^':
        n ^= operand;
        break;
    case '<':
        n <<= operand;
        break;
    case '>':
        n >>= operand;
        break;
    case 's':
        // for (int i = 31; i >= operand; i--)
        // {
        //     if(i == operand) getchar((n >> i & 1) == '0' ? '1' : '0');
        // }
        // break;
    // case 'd':
    //     n >>= operand;
    //     break;
    // case 'x':
    //     n >>= operand;
    //     break;
    default:
        printf("Operation or operand not allowed!\n");
        exit(0);
    }
    return n;
}

int main(void)
{
    unsigned int n = 0;
    unsigned int operand = 0;
    char operation;

    printBinaryNumbers(n);

    while (true)
    {
        scanf(" %c%d", &operation, &operand);

        n = calculateOperation(operation, operand, n);
        printBinaryNumbers(n);
    }
    return EXIT_SUCCESS;
}
