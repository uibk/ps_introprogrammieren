/* Includes the corresponding header files 
   to provide printf and EXIT_SUCCESS functionality 
*/
#include <stdio.h>
#include <stdlib.h>

/* main function where the application starts with return value
   int and void parameters */ 
int main(void) {
printf("Hello World\n");
return EXIT_SUCCESS;
}