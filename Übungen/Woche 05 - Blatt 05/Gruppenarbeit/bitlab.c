/*
	bitlab
*/
#include<stdio.h>
#include<stdlib.h>

/*
	- verwende digitN-Variablen nur in (1) und (2) 
	- verwende Bitoperatoren um (3) - (10) zu lösen

*/

int main(void){
	int number;
	char digit0, digit1, digit2;
	
	// (1) 3-stellige binäre Zahl eingeben und Ziffern in digitN-Variablen speichern
	// > 001
	scanf("%c%c%c", &digit2, &digit1, &digit0);
	
	// (2) Ziffern in Zahl umrechnen und Zahl in number speichern 
	number = ((digit2 == '1') << 2) | ((digit1 == '1') << 1 | ((digit0 == '1')));
	
	// (3) number als Dezimalzahl ausgeben 
	printf("Number is: %d\n", number);
	
	// (4) Ist number eine gerade Zahl? Ist number eine ungerade Zahl?
	printf("is even: %c\n", number & 1 ? 'N' : 'Y');
	
	// (5) Ist number > 3 ?
	printf("is bigger than 3: %c\n", number & 4 ? 'Y' : 'N');
	
	// (6) Berechne: Number * 4.
	printf("multiplied by 4: %d\n", number << 2);
	
	// (7) Ist number != 0 ?
	printf("!= 0: %c\n", number & 7 ? 'Y' : 'N');
	
	// (8) Ist number ein Palindrom? bspw. 101, 010, 000, 111
	printf("Palindrom: %c\n", (number >> 2 & 1) == (number & 1) ? 'Y' : 'N');

	
	// (9) Tausche das erste Bit mit dem letzen Bit.
	//     010 -> 010, 100 -> 001, 101 -> 101
	printf("number: %u\n", (number >> 2 & 1) | (number & 2) | ((number & 1) << 2));
	
	// (10) Flippe jedes Bit. 001 -> 110, 010 -> 101, 111 -> 000
	printf("flipped bits number: %d\n", 7 & ~number);
}