#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    int number;
	char digit0, digit1, digit2;
    // (1) 3-stellige binäre Zahl eingeben und Ziffern in digitN-Variablen speichern
	// > 001
	scanf("%c%c%c", &digit2, &digit1, &digit0);
	// (2) Ziffern in Zahl umrechnen und Zahl in number speichern 
	number = ((digit2 == '1') << 2 | ((digit1 == '1') << 1) | ((digit0 == '1')));
	// (3) number als Dezimalzahl ausgeben 
	printf("%d", number);
	// (4) Ist number eine gerade Zahl? Ist number eine ungerade Zahl?
	printf("is even: %c\n", number & 1 ? 'N' : 'Y');
	// (5) Ist number > 3 ?
	
	// (6) Berechne: Number * 4.
	
	// (7) Ist number != 0 ?

	// (8) Ist number ein Palindrom? bspw. 101, 010, 000, 111
	
	// (9) Tausche das erste Bit mit dem letzen Bit.
	//     010 -> 010, 100 -> 001, 101 -> 101
	
	// (10) Flippe jedes Bit. 001 -> 110, 010 -> 101, 111 -> 000
    return 0;
}

    
    