#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void)
{
    printf("Signed Char\n");
    printf("MinValue: %d\n", SCHAR_MIN);
    printf("MaxValue: %d\n", SCHAR_MAX);
    printf("-------------------------------\n");

    printf("Signed Short\n");
    printf("MinValue: %d\n", SHRT_MIN);
    printf("MaxValue: %d\n", SHRT_MAX);
    printf("-------------------------------\n");
    
    printf("Signed Int\n");
    printf("MinValue: %d\n", INT_MIN);
    printf("MaxValue: %d\n", INT_MAX);
    printf("-------------------------------\n");

    printf("Float\n");
    printf("MinValue: %f\n", FLT_MIN);
    printf("MaxValue: %f\n", FLT_MAX);
    printf("-------------------------------\n");

    printf("Double\n");
    printf("MinValue: %f\n", DBL_MIN);
    printf("MaxValue: %f\n", DBL_MAX);

    return 0;
}