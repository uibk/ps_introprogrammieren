#include <stdio.h>

int main(void)
{
    double totalPrice = 0.00;
    int fuelAmount = 0;
    long double fuelPrice = 0.00;
    char fuelInput = 'X';

    printf("Please insert amount of fuel: and fuel type e.g diesel = d or D\n");
    scanf("%d %c", &fuelAmount, &fuelInput);

    switch (fuelInput)
    {
    case 'd':
    case 'D':
        fuelPrice = 1.213;
        break;
    case 'b':
    case 'B':
        fuelPrice = 1.257;
        break;
    case 'S':
    case 's':
        fuelPrice = 1.358;
        break;
    default:
        printf("%s\n", "No fuel for given letter exist");
        return 0;
    }
    totalPrice = fuelPrice * fuelAmount;

    printf("Totalprice: %.2f Euro\n", totalPrice);
    return 0;
}
