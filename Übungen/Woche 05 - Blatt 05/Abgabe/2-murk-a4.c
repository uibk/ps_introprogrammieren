#include <stdio.h>

int main(void)
{

    int a = 31;
    int b = 50;

    if (a > b)
    {
        // Position 1
        printf("Position 1\n");
        if (a < 10)
        {
            // Position 2
            printf("Position 2\n");
        }
    }
    else if ((a < b) && !(a > 30))
    {
        if (a < 50 && b >= 50)
        {
            // Position 3
            printf("Position 3\n");
        }
        else
        {
            // Position 4
            printf("Position 4\n");
        }
    }
    else
    {
        // Position 5
        printf("Position 5\n");
        if (4 & 2)
        {
            // Position 6
            printf("Position 6\n");
        }
        else
        {
            // Position 7
            printf("Position 7\n");
        }
    }
}