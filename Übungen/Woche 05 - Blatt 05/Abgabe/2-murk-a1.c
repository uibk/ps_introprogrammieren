#include <stdio.h>

int main(void)
{
    int n = 0;
    int index = 1;
    int tempIndex = 1;

    printf("Type in n:\n");
    scanf("%d", &n);

    while (index < (n * 2))
    {
        if (index % 2 != 0)
        {
            for (int j = 1; j <= tempIndex; j++)
            {
                printf("%d ", j);
            }
            printf("\n");
            tempIndex++;+
        }
        else
        {
            for (int i = 1; i <= n; i++)
            {
                printf("%d ", i);
            }
            printf("\n");
        }
        index++;
    }
    return 0;
}
