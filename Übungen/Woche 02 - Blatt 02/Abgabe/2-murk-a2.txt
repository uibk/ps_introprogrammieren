Aufgabe 2)

1:
C-Source Code wird von einem Programmierer geschrieben, der dann später in
Maschinencode (Byte-Code) also 0 und 1 über gcc (beispielsweise) also über einen Compiler 
übersetzt und anschließend in Maschinencode übersetzt wird.
Source-Code -> Programmcode des Developers
Executable -> ausführbare Datei die mit Maschinensprache übersetzt wurde

2:
Beim Kompilieren wird der geschriebene Code zuerst in Assembly Directives 
übersetzt. Diese sind einfache Methoden wie ADD, MOVE zum Adressieren der
Speicheradressen. Es stellt eine symbolische Repräsentation des Maschinencodes
an. Anschließend wird der assembly Code dann über einen Assembler in den
Maschinencode übersetzt. Dieser kann in den Speicher geladen und 
ausgeführt werden.

3: 
-Wall gibt alle Warnungen aus, die z. B. auf eine nicht verwendete Variable
dem User hinweisen. Auch über weitere Konstruktionen wird der User über dieses Flag gewarnt.
-Werror verwandelt anschließend ALLE Warnungen in Errors, damit das Programm je nach Schweregrad
gar nicht zum Kompilieren kommt und der Fehler beseitigt werden muss.

4:
#include <stdio.h>
#include <stdlib.h>

Hier werden die benötigten Libraries mit dem #include statement hineingeladen und verwendet.
Zur Laufzeit wird ein Präprozessor diese Binaries analysieren und sprichwörtlich hineinkopieren,
damit zur Laufzeit diese Bibliotheken im Code verwendet werden. stdio.h ist für die printf Funktion
und stdlib.h steht für die Konstante EXIT_SUCCESS

5:
Das executable wird im gleichen Verzeichnis nach dem Kompilieren erzeugt und kann anschließend
einfach mit ./a2 ausgeführt werden und der Text wird anschließend in der Konsole ausgegeben.
