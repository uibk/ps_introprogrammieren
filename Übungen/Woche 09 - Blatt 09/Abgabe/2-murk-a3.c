#include <stdlib.h>
#include <stdio.h>
#define SIZE 5

void checkCommand(char command, int array[SIZE][SIZE]);
void checkAdminInput();
void doReservation();
void cancelReservation();
void displayReservation();
void printStartScreen();

void printStartScreen()
{
    printf("Welcome to table reservation:\n");
    printf("R: Reserve a table\n");
    printf("C: Cancel a reservation\n");
    printf("D: Display all reservations\n");
}

void checkAdminInput(int array[SIZE][SIZE])
{
    char c = '\0';

    do
    {
        scanf(" %c", &c);
        checkCommand(c, array);
    } while ((c = getchar()) != EOF || (c = getchar()) != '\n');
}

void checkCommand(char command, int array[SIZE][SIZE])
{
    switch (command)
    {
    case 'R':
    case 'r':
        doReservation(array);
        break;
    case 'C':
    case 'c':
        cancelReservation(array);
        break;
    case 'D':
    case 'd':
        displayReservation(array);
        break;
    default:
        printf("Bye!\n");
        exit(0);
    }
}

void doReservation(int array[SIZE][SIZE])
{
    int indexFirstDimension = 0;
    int indexSecondDimension = 0;
    int reservations = 0;

    scanf("%d %d %d", &indexFirstDimension, &indexSecondDimension, &reservations);
    array[indexFirstDimension - 1][indexSecondDimension - 1] = reservations;

    printf("Success!\n");
}

void cancelReservation(int array[SIZE][SIZE])
{
    int indexFirstDimension = 0;
    int indexSecondDimension = 0;

    scanf("%d %d", &indexFirstDimension, &indexSecondDimension);
    array[indexFirstDimension - 1][indexSecondDimension - 1] = 0;

    printf("Success!\n");
}

void displayReservation(const int array[SIZE][SIZE])
{
    printf("Current reservation(s):\n");
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            printf("%d   ", array[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{
    int tables[5][5] = {
        0};

    printStartScreen();
    checkAdminInput(tables);
}