#include <stdlib.h>
#include <stdio.h>

int checkArr(const int arr1[], const int arr2[]) {
    int diff = 0;
    for(int i = 0; i < 4; i++) {
        if(arr1[i] != arr2[i]) {
            diff++;
        }
    }
    return diff;
}

int main(void) {

    int arr1[4] = {1, 2, 3};    
    int arr2[4] = {1, 2, 3};    

    // old version
    // if(arr1 == arr2) {
    //     printf("Same array.");
    // } else {
    //     printf("Different array.");
    // }

    //solution
    if(checkArr(arr1, arr2) == 0 ? printf("Same array.") : printf("Different array."));
    return 0;
}

// Problem is that when you use == equal then you would only compare the two
// adresses of each array at it's beginning