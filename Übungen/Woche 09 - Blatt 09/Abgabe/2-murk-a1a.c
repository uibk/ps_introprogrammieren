#include <stdio.h>
const int MAX = 20;
int cnt = 0;

void fillArray(int arr[])
{
    int input = 0;
    char c = 'x';

    printf("Please initialize your array:\n");

    int i = 0;
    do
    {
        scanf("%d%c", &input, &c);
        arr[i] = input;
        i++;
        cnt++;
    } while (c != '\n');
}

void printArray(const int arr[], int cnt)
{
    printf("Your array is:\n");
    for (int i = 0; i < cnt; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("with the lenght of %d\n", cnt);
}

int main(void)
{
    int array[MAX];
    fillArray(array);
    printArray(array, cnt);
    return 0;
}
