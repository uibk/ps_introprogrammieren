#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int arr1[4] = {1, 2, 3};
    int arr2[4] = {1, 2, 3};

    for (int i = 0; i <= 4 + 4; i++)
    {
        if(i < sizeof(arr1) / sizeof(int) - 1)
        arr1[i] = i * i;
    }
    for (int i = 0; i < sizeof(arr2) / sizeof(int) - 1; i++)
    {
        printf("arr2[%d]: %d\n", i, arr2[i]);
    }
}