#include <stdio.h>
const int MAX = 20;
int cnt = 0;

void fillArray(int arr[])
{
    int input = 0;
    char c = 'x';

    printf("Please initialize your array:\n");

    int i = 0;
    do
    {
        scanf("%d%c", &input, &c);
        arr[i] = input;
        i++;
        cnt++;
    } while (c != '\n');
}

void printArray(const int arr[], int cnt)
{
    printf("Your array is:\n");
    for (int i = 0; i < cnt; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("with the lenght of %d\n", cnt);
}

void sortArray(int arr[], int cnt)
{
    printf("Start to sorting the array...\n");
    for (int i = cnt - 1; i >= 1; i--)
    {
        for (int j = 0; j <= i; j++)
        {
            if (j + 1 < cnt)
            {
                int indexValue = arr[j];
                int afterIndexVal = arr[j + 1];

                if (afterIndexVal > indexValue)
                {
                    arr[j] = afterIndexVal;
                    arr[j + 1] = indexValue;
                }
            }
        }
    }

    printArray(arr, cnt);
}

int main(void)
{
    int array[MAX];
    fillArray(array);
    sortArray(array, cnt);
    return 0;
}
