#include <stdlib.h>
#include <stdio.h>

char *dna_strand(const char *dna)
{
    char * dna_array;
    dna_array = malloc(sizeof(char) * 5);
    for (int i = 0; i < (sizeof(dna) / sizeof(char)); i++)
    {
        switch (dna[i])
        {
        case 'A':
            dna_array[i] = 'T';
            break;
        case 'T':
            dna_array[i] = 'A';
            break;
        case 'G':
            dna_array[i] = 'C';
            break;
        case 'C':
            dna_array[i] = 'G';
            break;
        default:
            break;
        }
    }
    return dna_array;
}

int main(void)
{
    printf("%s", dna_strand("GTAT"));
    return 0;
}