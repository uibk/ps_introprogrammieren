#include <stdio.h>
#include <stdlib.h>

void printArray(int size, int arr[size][size], char message[]);

void transpose(int size, int matrix[size][size], char message[])
{
    int tempColumn = 0;
    int tempRow = 0;
    int newArray[size][size];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            tempColumn = matrix[j][i];
            tempRow = matrix[i][j];

            newArray[i][j] = tempColumn;
            newArray[j][i] = tempRow;
        }
    }
    printArray(size, newArray, message);
}

void printArray(int size, int arr[size][size], char message[])
{
    printf("%s \n", message);

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{
    int matrix[4][4] = {
        {1, 3, 4, 9},
        {2, 5, 11, 6},
        {26, 12, 1, 21},
        {10, 28, 15, 17}};

    int size = (sizeof(matrix[0]) / sizeof(int));

    char originalMatrix[] = "The original matrix is:";
    char transposeIs[] = "The transpose is:";

    printArray(size, matrix, originalMatrix);
    transpose(size, matrix, transposeIs);
    return 0;
}
