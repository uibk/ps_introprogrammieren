#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int arr1[4] = {1, 2, 3};
    printf("There are %ld elements in the array\n",
           sizeof(arr1) / sizeof(int) - 1);
}