#include <stdio.h>
const int MAX = 20;
int cnt = 0;

void fillArray(int arr[])
{
    int input = 0;
    char c = 'x';

    printf("Please initialize your array:\n");

    int i = 0;
    do
    {
        scanf("%d%c", &input, &c);
        arr[i] = input;
        i++;
        cnt++;
    } while (c != '\n');
}

void printArray(const int arr[], int cnt)
{
    printf("Your array is:\n");
    for (int i = 0; i < cnt; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("with the lenght of %d\n", cnt);
}

void insertArray(int arr[], int cnt)
{
    int input = 0;
    int index = 0;
    printf("Please provide the value to be inserted\n");
    scanf("%d", &input);
    printf("Please specify the index of the position\n");
    scanf("%d", &index);
    arr[index] = input;
}

int main(void)
{
    int array[MAX];
    fillArray(array);
    insertArray(array, cnt);
    printArray(array, cnt);
    return 0;
}
