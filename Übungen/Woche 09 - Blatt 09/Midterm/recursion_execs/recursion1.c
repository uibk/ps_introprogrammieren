#include <stdlib.h>
#include <stdio.h>

void list(int start, int end)
{
    if (start > 0 && start <= end)
    {
        printf("%d ", start);
        list(start + 1, end);
    }
}

int main(void)
{

    int n = 0;
    int m = 0;
    printf("Type in your numbers: ");
    scanf("%d %d", &n, &m);
    list(n, m);
    printf("\n");

    return 0;
}
