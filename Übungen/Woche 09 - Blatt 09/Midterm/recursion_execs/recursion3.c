#include <stdlib.h>
#include <stdio.h>

int temp = 0;

void draw(unsigned int number)
{
    if (number > 0)
    {
        temp += 1;
        number--;
        printf("%d ", temp);
        draw(number);
        temp--;
        printf("%d ", temp+1);
    }
}

int main(void)
{
    int input = 0;
    printf("Type in number: ");
    scanf("%d", &input);
    draw(input);
    printf("\n");
    return 0;
}