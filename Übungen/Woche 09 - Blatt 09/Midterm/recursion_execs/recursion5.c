#include <stdlib.h>
#include <stdio.h>

int start = 1;
int gap = 1;

void calcGapNumbers(int start, int gap)
{
    if (gap <= 10)
    {
        printf("%d ", start);
        start += gap;
        gap++;
        calcGapNumbers(start, gap);
    }
}

void is_gapNumber(unsigned int number)
{
    if(start <= number) {
        printf("%d ", start);
        start += gap;
        gap++;
        if(start <= number) {
            calcGapNumbers(start, gap);
        } else if (start == number) {
            printf("%d ist eine Abstandszahl", number);
            return;
        } else {
            printf("%d ist keine Abstandszahl", number);
            return;
        }
    }
}

int main(void)
{
    // int number = 1;
    // int gap = 1;
    // calcGapNumbers(number, gap);
    is_gapNumber(11);
    printf("\n");
    return 0;
}