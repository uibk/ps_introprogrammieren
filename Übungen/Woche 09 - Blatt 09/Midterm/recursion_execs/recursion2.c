#include <stdlib.h>
#include <stdio.h>

void force_input(char c)
{
    char input = 'X';
    scanf(" %c", &input);

    if (c == input)
    {
        printf("OK\n");
    }
    else
    {
        printf("WRONG\n");
        force_input(c);
    }
}

int main(void)
{
    force_input('g');
    force_input('o');
    force_input('!');
    return 0;
}