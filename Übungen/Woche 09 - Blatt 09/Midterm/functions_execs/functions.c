#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int input_number(int start, int end)
{
    int input = 0;
    printf("Please enter a number between %d and %d\n", start, end);
    scanf(" %d", &input);
    if (input <= end && input >= start)
    {
        return input;
    }
    else
    {
        input_number(start, end);
    }
    return input;
}

void printn(char c, int number)
{

    for (int i = 1; i <= number; i++)
    {
        printf("%c ", c);
    }
}

int char2num(char c)
{
    return (int)c;
}

char num2char(int number)
{
    return (char)number;
}

int random_number(int n, int m)
{
    int randomNr = rand() % (m - n + 1) + n;
    return randomNr;
}

int main(void)
{
    int start = 1;
    int count10 = 0;
    int count11 = 0;
    int count12 = 0;
    int randNumber = 0;

    srand(time(NULL)); // Only call once, otherwise it will print out same number every time

    // printf("Die Zahl ist %d\n", input_number(5, 12));
    // printn('#', 3);
    // printf("%d\n", char2num(num2char(6)));
    while (start <= 50)
    {
        // printf("Start: #%d\n", start);
        randNumber = random_number(10, 12);

        switch (randNumber)
        {
        case 10:
            count10++;
            break;
        case 11:
            count11++;
            break;
        case 12:
            count12++;
            break;
        default:
            break;
        }
        start++;
    }
    printf("10 comes %d times\n", count10);
    printf("11 comes %d times\n", count11);
    printf("12 comes %d times\n", count12);
   
    return 0;
}