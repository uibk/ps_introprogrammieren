#include <stdlib.h>
#include <stdio.h>

void print_upwards(int howMany, int number)
{
    for (int i = 1; i <= howMany; i++, number++)
    {
        printf("%d ", number);
    }
}

void print_downwards(int howMany, int zahl)
{
    for (int i = 1; i <= howMany; i++, zahl--)
    {
        printf("%d ", zahl);
    }
}

void print_char(int howMany, char character)
{
    for (int i = 0; i <= howMany; i++)
    {
        printf("%c ", character);
    }
}

int main(void)
{
    int startNumber = 0;
    printf("Type in your number: ");
    scanf("%d", &startNumber);

    int n = startNumber;
    int upCounter = 0;

    for (int i = 1; i <= startNumber; i++, n--, upCounter++)
    {
        print_char(startNumber - (i)-1, ' ');
        print_downwards(upCounter, startNumber);
        print_upwards(i, n);
        printf("\n");
    }

    // Set to 0 to use it in second for loop for incrementing again
    upCounter = 0;
    int x = 2;

    for (int i = startNumber - 1; i >= 1; i--, x++, upCounter++)
    {
        print_char(upCounter, ' ');
        print_downwards(i - 1, startNumber);
        print_upwards(i, x);
        printf("\n");
    }

    return 0;
}