#include <stdlib.h>
#include <stdio.h>

void print_times(char c, int number)
{
    for (int i = 1; i <= number; i++)
    {
        printf("%c ", c);
    }
}

int main(void)
{

    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);
    for (int i = 1; i <= number; i++)
    {
        print_times('0' + i, i);
        print_times(' ', (number * 2) - (i * 2));
        print_times('0' + i, i);
        printf("\n");
    }
    for (int i = number - 1; i >= 1; i--)
    {
        print_times('0' + i, i);
        print_times(' ', (number * 2) - (i * 2));
        print_times('0' + i, i);
        printf("\n");
    }

    return 0;
}