#include <stdlib.h>
#include <stdio.h>

void print_times(char c, int number)
{
    for (int i = 1; i <= number; i++)
    {
        printf("%c ", c);
    }
}
void print_char(char c, int number)
{
    for (int i = 1; i <= number; i++)
    {
        printf("%c", c);
    }
}

int main(void)
{
    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);
    int n = number;
    for (int i = 1; i <= number; i++)
    {
        print_char(' ', n-1);
        print_times('0' + i, i);
        printf("\n");
        n--;
    }

    return 0;
}