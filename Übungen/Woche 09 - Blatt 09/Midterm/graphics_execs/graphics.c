#include <stdlib.h>
#include <stdio.h>

void print_times(char c, int number) {
    for(int i = 1; i <= number; i++) {
        printf("%c ", c);
    }
}

int main(void)
{
    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);
    int n = 0;
    for(int i = 1; number >= 1; number--, i++) {
        print_times(' ', n);
        print_times('0' + i, number);
        printf("\n");
        n++;
    }
    return 0;
}