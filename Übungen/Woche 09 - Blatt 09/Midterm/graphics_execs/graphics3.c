#include <stdlib.h>
#include <stdio.h>

void print_times(int number)
{
    for (int i = 1; i <= number; i++)
    {
        printf("%d ", i);
    }
}

int main(void)
{

    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);

    for (int i = 1; i <= number; i++)
    {
        print_times(i);
        printf("\n");
    }

    for (int i = number - 1; i >= 1; i--)
    {
        print_times(i);
        printf("\n");
    }
    return 0;
}