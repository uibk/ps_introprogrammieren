#include <stdlib.h>
#include <stdio.h>

void print_times(int times)
{
    for (int i = 1; i <= times; i++)
    {
        printf("%d ", i);
    }
}
void print_char(char c, int times)
{
    for (int i = 1; i <= times; i++)
    {
        printf("%c ", c);
    }
}

int main(void)
{
    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);
    int n = number -1;
    for (int i = 1; i <= number; i++)
    {
        print_char(' ', n);
        print_times(i);
        printf("\n");
        n--;
    }

    return 0;
}