#include <stdlib.h>
#include <stdio.h>

void print_times(int number, int idk)
{
    for (int i = 1; i <= number; i++, idk--)
    {
        printf("%d ", idk);
    }
}

void print_number(int number, int idk)
{

    for (int i = 1; i <= number; i++)
    {
        printf("%d ", idk);
        idk++;
    }
}

void print_char(char c, int number)
{
    for (int i = 1; i <= number; i++)
    {
        printf("%c ", c);
    }
}

int main(void)
{
    int number = 0;
    printf("Type in your number: ");
    scanf("%d", &number);
    int cnt = 0;
    int n = number;

    for (int i = 1; i <= number; i++)
    {
        print_char(' ', number - (1 * i));
        print_number(cnt, n);
        n--;
        print_times(i, number);

        printf("\n");
        cnt++;
    }

    return 0;
}