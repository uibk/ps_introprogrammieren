#include <stdio.h>

void calcHailstoneSequence(int n)
{
    if (n != 1)
    {
        if (n % 2 == 0)
        {
            n = n / 2;
            printf("%d, ", n);
        }
        else
        {
            n = (3 * n) + 1;
            printf("%d, ", n);
        }
        calcHailstoneSequence(n);
    }
}

int main(void)
{
    int value = 0;
    printf("Tell me the number: ");
    scanf(" %d", &value);
    calcHailstoneSequence(value);
    printf("\n");
    return 0;
}