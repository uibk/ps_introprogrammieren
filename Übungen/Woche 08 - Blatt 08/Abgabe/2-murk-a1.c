#include <stdio.h>
#include <stdbool.h>

int sum = 0;
int start = 1;

int isPerfect(int number, int sum)
{
    return (number == sum) ? printf("%d is a Prime number\n", number) : 0;
}

void printPerfect(int start, int end)
{
    if (end <= 100000)
    {
        for (; start <= end; start++)
        {
            for (int i = 1; i < start; i++)
            {
                if (start % i == 0)
                {
                    sum += i;
                }
            }
            isPerfect(start, sum);
            sum = 0;
        }
    }
}

int main(void)
{
    int end = 0;

    printf("Tell me the start and the end of your desired numbers: ");
    scanf("%d %d", &start, &end);
    printPerfect(start, end);
    
    return 0;
}