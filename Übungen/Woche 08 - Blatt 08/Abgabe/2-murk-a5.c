#include <stdio.h>

// (A)
// int f2(int a)
// {
//     return (a * a);
// }

// int f1(int a, int b)
// {
//     return (f2(20));
// }

// (B)
// int a = 50;

// (D)
int i;

int main(void)
{
    // (A)
    // f1(5, 20);

    // (B)
    // int b = 40;

    // (C)
    // int x = 1, y = 2, z = 3;
    // printf(" x = %d, y = %d, z = %d \n", x, y, z);
    // {
    //     int x = 10;
    //     float y = 20;
    //     printf(" x = %d, y = %f, z = %d \n", x, y, z);
    //     {
    //         int z = 100;
    //         printf(" x = %d , y = %f, z = %d \n", x, y, z);
    //     }
    // }

    // (D)
    // extern int i;
    // if(i == 0) printf("scope rules\n");
    // return 0;
   
}
// (B)
// int c = 40;

// (A)
// It can be only executed, when f2 is declared first! Otherwise, f1 function cannot find f2();

// (B)
// a is a global variable and b is a local variable. Normally, c is a global variable as well but cannot be used in main because it's declared afterwards.
// Therefore, a has the highest scope as global variables can be used everywhere.

// (C)
// Output:
//  x = 1, y = 2, z = 3
//  x = 10, y = 20.000000, z = 3
//  x = 10 , y = 20.000000, z = 100

// (D)
// scopes rules was printed out because trough extern definition you initialize the variable with a default value and allocate memory to it trough extern.
// The compiler uses the extern keyword (hidden) for every function declaration and definition to define their accessible scope
// with extern we can declare a variable in C without defining it (passing a value to it) to use the variable.