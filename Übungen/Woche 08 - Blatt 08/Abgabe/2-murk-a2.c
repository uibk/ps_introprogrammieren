#include <stdio.h>
#include <math.h>
#include "fact.h"

int fact(int number)
{
    if (number >= 1)
    {
        return number * fact(number - 1);
    }
    else
    {
        return 1;
    }
}
int started = 0;
int startedCos = 0;

double calcCos(double x, double n)
{
    if (x > 0.000001)
    {
        if (started == 0)
        {
            started = 1;
            if (startedCos == 0)
            {
                startedCos = 1;
                x = 1 - pow(x, n) / fact(n);
            }
            else
            {
                x -= pow(x, n) / fact(n);
            }
        }
        else if (started == 1)
        {
            started = 0;
            x += pow(x, n) / fact(n);
        }
        calcCos(x, n + 2);
    }
    return x;
}
double calcSin(double x, double n)
{
    if (x > 0.000001)
    {
        if (started == 0)
        {
            started = 1;
            x -= pow(x, n) / fact(n);
        }
        else if (started == 1)
        {
            started = 0;
            x += pow(x, n) / fact(n);
        }
        calcSin(x, n + 2);
    }
    return x;
}

int main(void)
{
    float value = 0;
    printf("Tell me the number: ");
    scanf("%f", &value);

    printf("Cosin of %.4f = %.4f\n", value, calcCos(value, 2));
    printf("Sine of %.4f = %.4f\n", value, calcSin(value, 3));
    
    return 0;
}