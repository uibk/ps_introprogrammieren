#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

int myRand(int low, int high)
{
    srand(time(NULL));
    return rand() % (high - low + 1) + low;
}

double withdrawMoney(double balance)
{
    double input = 0.0;
    printf("How much money do you want to withdraw? ");
    scanf("%lf", &input);
    if (input <= balance)
    {
        balance -= input;
        printf("New Balance: %.2lf €\n", balance);
    }
    else
    {
        printf("Withdraw not possible!\n");
    }
    return balance;
}

double depositMoney(double balance)
{
    double input = 0.0;
    printf("How much money do you want to deposit? \n");
    scanf("%lf", &input);
    balance += input;
    printf("New Balance: %.2lf €\n", balance);
    return balance;
}

void checkBalance(double balance)
{
    printf("Current account balance: %.2f €\n", balance);
}

int checkPIN(int PIN)
{
    int inputPIN = 0;
    printf("Please enter your 4-digit long PIN: ");
    scanf("%d", &inputPIN);
    if (PIN == inputPIN)
        return true;
    return false;
}

int randomPIN()
{
    srand(time(NULL));
    return myRand(1000, 9999);
}

double randomBalance()
{
    srand(time(NULL));
    return myRand(5000, 50000);
}

void printInstructions()
{
    printf("Welcome to ATM Inc.!\n\n\n");
}
void printHelp()
{
    printf("---------------------------------------\n");
    printf("Type [q] for exit \n");
    printf("Type [b | B] for checking balance \n");
    printf("Type [w | W] for withdraw money \n");
    printf("Type [d | D] for deposit money \n");
    printf("---------------------------------------\n");
}

void simulateBanking(int PIN, double balance)
{
    char input = 'X';

    if (checkPIN(PIN))
    {
        printf("Type [h | H] for help instructions \n");

        while (input != 'q')
        {
            printf("What do you wanna do? \n");
            scanf(" %c", &input);

            switch (input)
            {
            case 'B':
            case 'b':
                checkBalance(balance);
                break;
            case 'W':
            case 'w':
                balance = withdrawMoney(balance);
                break;
            case 'D':
            case 'd':
                balance = depositMoney(balance);
                break;
            case 'h':
            case 'H':
                printHelp();
                break;
            default:
                break;
            }
        }
        printf("Bye!\n");
    }
    else
    {
        printf("PIN is invalid!\n");
    }
}

int main(void)
{
    int PIN = randomPIN();
    double balance = randomBalance();

    printInstructions();

    printf("PIN: %d\n", PIN);
    printf("Balance: %.2f €\n", balance);

    simulateBanking(PIN, balance);
    return 0;
}