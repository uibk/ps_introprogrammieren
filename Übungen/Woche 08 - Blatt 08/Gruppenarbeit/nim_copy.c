/*
	NIM
	
	Das Nim-Spiel ist ein Spiel für zwei Personen, bei dem abwechselnd eine Anzahl von Gegenständen, etwa Streichhölzer, weggenommen werden. Gewonnen hat beim Standardspiel derjenige, der das letzte Hölzchen nimmt. -- https://de.wikipedia.org/wiki/Nim-Spiel
	
	(1) Kompiliere das Spiel und starte es.
	
	(2) Versuche den Code zu verstehen.
*/

#include <stdio.h>
#include <stdlib.h>

// definiere einen neuen Datentyp "Player" als "enum {PLAYER_A, PLAYER_B}" 
typedef enum {PLAYER_A, PLAYER_B} Player;

unsigned char const max_objects_per_turn = 3;

void print_intro();

int main(void){
	unsigned char objects = 10;
	Player player = PLAYER_A;
	
	print_intro();
	
	// (3) Erstelle eine Funktion void print_objects(int objects) welche die Streichhölzer ausgibt.
	// bei print_objects(5) wird bspw. folgendes ausgegeben:
	//
	// | | | | |
	//
	printf("Streichhölzer: %d\n", objects);
	
	// (7) Schreibe das Spiel um, sodass sich die Funktion Player turn(Player player, int objects) sich rekursiv solange aufruft, bis ein Gewinner feststeht. Die Funktion gibt den Gewinner zurück.
	while(objects > 0){
		int input;
		player = player == PLAYER_A ? PLAYER_B : PLAYER_A;		
		
		printf("---------------------------");
		
		// (4) Erstelle eine Funktion char char_of_player(Player player) welche je nach Spieler 'A' oder 'B' zurückgibt.
		printf("Spieler: %c\n", player == PLAYER_A ? 'A' : 'B');	
		printf("Streichhölzer: %d\n", objects);
		printf("\n");
		
		// (5) Erstelle eine Funktion int input_objects(Player player, int objects) welche einliest wieviele Streichhölzer der Spieler nimmt. Die Funktion gibt die Anzahl der genommenen Streichhölzer zurück.
		// (6) Es soll nur möglich sein 1 bis maximal 3 Streichhölzer zu nehmen (je nachdem wieviele noch da sind). Es muss mindestens 1 Streichholz genommen werden. Bei ungültiger Eingabe wird nochmals gefragt.
		printf("NIM: ");
		scanf(" %d", &input);
		objects -= input;
		printf("\n");
	}
	
	printf("Spieler %c hat gewonnen\n", player == PLAYER_A ? 'A' : 'B');
		
	return EXIT_SUCCESS;
}



void print_intro(){
	printf("   |                   \n");
	printf("  |||    NIM - The Game\n");
	printf(" |||||                 \n");
	printf("\n");
}