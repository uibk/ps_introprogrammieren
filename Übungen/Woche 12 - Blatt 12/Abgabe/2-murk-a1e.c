#include <stdlib.h>
#include <stdio.h>

int oddsum(int *a, int n)
{
    int i = -1;
    int sum;
    while (i < n)
    {
        i += 2;
        sum += a[i];
    }
    return sum;
}

int main(void)
{
    int size = 15;
    int *a = malloc(sizeof(int) * size);
    for (int i = 0; i < size; i++)
    {
        a[i] = i;
    }
    int res = oddsum(a, size);
    printf("%d\n", res);
    free(a);
    a = malloc(sizeof(int) * size);
    for (int i = 0; i < size; i++)
    {
        a[i] = i * 2;
    }
    free(a);
    res = oddsum(a, size);
    printf("%d\n", res);
    return EXIT_SUCCESS;
}