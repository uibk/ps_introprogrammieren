#include <stdio.h>
#include <stdlib.h>

typedef struct _list_element
{

} list_element_t;

typedef struct
{

} list_t;


list_element_t* list_insert(list_t* list, list_element_t* elem, void* data)
{
    return NULL;
}

void* list_remove(list_t* list, list_element_t* elem)
{
    return NULL;
}

void list_iterate(list_t* list, void (*func)(list_element_t* elem))
{

}

void list_push_front(list_t* list, void* data)
{

}

void* list_get_front(list_t* list)
{
    return NULL;
}

void* list_get_back(list_t* list)
{
    return NULL;
}

void* list_get_at(list_t* list, unsigned int pos)
{
    return NULL;
}

list_t* list_init(void)
{
    return NULL;
}

void list_destroy(list_t* list, void (*data_free)(void* data))
{

}

/*********** Test **********/
typedef struct
{
    unsigned int m_number;
    char m_name[16];
} student_t;

void print_student(list_element_t* elem)
{

}

void print_int(list_element_t* elem)
{

}

#define TEST(x) do { if(!(x)) { fprintf(stderr, "(%s) failed in line %d\n", #x, __LINE__); exit(EXIT_FAILURE); } } while(0)

int main(int argc, char** argv)
{
    {
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;
        int e = 5;

        list_t* list = list_init();
        list_push_front(list, &e);
        list_push_front(list, &d);
        list_push_front(list, &c);
        list_push_front(list, &b);
        list_push_front(list, &a);

        TEST(list_get_front(list) == &a);
        TEST(list_get_back(list) == &e);
        TEST(list_get_at(list, 2) == &c);

        list_iterate(list, print_int);

        list_destroy(list, NULL);
    }

    printf("\n\n");

    {
        student_t* a = malloc(sizeof(student_t));
        *a = (student_t) { 1, "Matthias" };

        student_t* b = malloc(sizeof(student_t));
        *b = (student_t) { 4, "Sebastian" };

        student_t* c = malloc(sizeof(student_t));
        *c = (student_t) { 123, "Peter" };

        list_t* list = list_init();
        list_push_front(list, a);
        list_push_front(list, b);
        list_push_front(list, c);

        TEST(list_get_front(list) == c);
        TEST(list_get_back(list) == a);
        TEST(list_get_at(list, 1) == b);

        list_iterate(list, print_student);

        list_destroy(list, free);
    }

    return EXIT_SUCCESS;
}
/*********** Test **********/

