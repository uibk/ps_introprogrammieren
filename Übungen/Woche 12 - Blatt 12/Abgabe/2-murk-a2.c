#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

typedef struct
{
    char riff_tag[4];
    uint32_t file_size;
    char wave_tag[4];
    char fmt_tag[4];
    uint32_t fmt_length;
    uint16_t format_tag;
    uint16_t channels;
    uint32_t sample_rate;
    uint32_t frame_size;
    uint16_t sample_alignment;
    uint16_t bits_per_sample;
    uint32_t data_header;
    uint32_t data_size;
} wav_header_t;

void print_information(wav_header_t header, char const *filePath);

int main(int argc, char const *argv[])
{
    FILE *file;
    wav_header_t header;
    file = fopen(argv[1], "r+b");

    if (file != NULL)
    {
        fread(&header, sizeof(header), 1, file);
    }
    else
    {
        printf("cannot open file!\n");
        exit(0);
    }
    print_information(header, argv[1]);
    fclose(file);
    return 0;
}

void print_information(wav_header_t header, char const *filePath)
{
    printf("filepath: %s\n", filePath);
    printf("RIFF Magic Number = %s\n", header.riff_tag);
    printf("file size = %d kb\n", header.file_size / 1000);
    printf("WAVE = %s\n", header.wave_tag);
    printf("format header = %s\n", header.fmt_tag);
    printf("size of format data in bytes = %d\n", header.fmt_length);
    if (header.format_tag == 1)
    {
        printf("type of format = PCM\n");
    }
    printf("channels = %d\n", header.channels);
    printf("sample rate = %d hz\n", header.sample_rate);
    printf("frame size = %d\n", header.frame_size);
    printf("sample alignment = %d\n", header.sample_alignment);
    printf("bits per sample = %d\n", header.bits_per_sample);
    printf("data header = %d\n", header.data_header);
    printf("data size = %d\n", header.data_size);
    printf("runtime = %.2f s\n", (double) header.file_size / (double) header.frame_size);
}
