#include <stdio.h>
#include <stdlib.h>

void zip(char *firstString, char *secondString);

int main(int argc, char *argv[])
{
    if (argc >= 3)
    {
        zip(argv[1], argv[2]);
    }
    else
    {
        printf("invalid call, use: zipper <first string> <second string>\n");
    }

    return 0;
}

void zip(char *firstString, char *secondString)
{
    int i = 0;
    char *ptr1 = firstString;
    char *ptr2 = secondString;

    while ((*ptr1 != 0) || (*ptr2 != 0))
    {
        printf("%c%c", *(ptr1++), *(ptr2++));
        i++;
    }
    printf("\n");
}