#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <curses.h>

#define TIME_DELAY 150

#define FIELD_X 60
#define FIELD_Y 20

#define CHAR_EMPTY      ' '
#define CHAR_SNAKE_HEAD '@'
#define CHAR_SNAKE_TAIL '#'
#define CHAR_FOOD       '*'

struct Vector2D
{
    int x, y;
};

struct Snake
{

};


WINDOW* init_screen(char field[FIELD_Y][FIELD_X]);
void shutdown(WINDOW* window);
void show_game_over(WINDOW* window, struct Snake* snake);


void place_food(struct Snake* snake, char field[FIELD_Y][FIELD_X])
{
    field[0][0] = CHAR_FOOD;
}

void draw(WINDOW* window, struct Snake* snake,  char field[FIELD_Y][FIELD_X])
{
    /*  draw field */
    for(int i = 0; i < FIELD_Y; i++)
    {
        for(int j = 0; j < FIELD_X; j++)
        {
            mvwaddch(window, i + 1, j + 1, field[i][j]);
        }
    }

    /* draw snake */


    /* draw window frame with title and info */
    box(window, 0, 0);
    mvwprintw(window, 0, FIELD_X/2 - 9,  "[ Terminal-Snake ]");
    mvwprintw(window, FIELD_Y+1, FIELD_X - 14, "[ Points: 000 ]");

    wrefresh(window);
    refresh();
}

void update(struct Snake* snake, char field[FIELD_Y][FIELD_X], bool* game_over)
{

}

void handle_input(struct Snake* snake)
{
    // see http://www.cs.ukzn.ac.za/~hughm/os/notes/ncurses.html#input
}

int main(int argc, char** argv)
{
    char field[FIELD_Y][FIELD_X] = { {'\0'} };
    struct Snake snake = {  };
    bool game_over = false;
    WINDOW* window = init_screen(field);

    /* place initial food on field */
    place_food(&snake, field);

    /* game loop */
    while(!game_over)
    {
        // read player input
        handle_input(&snake);

        // update game logic (apple, snake)
        update(&snake, field, &game_over);

        // draw game field and snake
        draw(window, &snake, field);
    }

    show_game_over(window, &snake);
    shutdown(window);

    return EXIT_SUCCESS;
}





void show_game_over(WINDOW* window, struct Snake* snake)
{
    mvwprintw(window, FIELD_Y/2 - 1, FIELD_X/2 - 4, "Game Over!");
    mvwprintw(window, FIELD_Y/2, FIELD_X/2 - 8, "Final Score: 0");
    wrefresh(window);

    nodelay(stdscr, FALSE);
    getch();
}

WINDOW* init_screen(char field[FIELD_Y][FIELD_X])
{
    srand(time(NULL));

    /* init curses system */
    initscr();
    raw();
    nodelay(stdscr, TRUE);
    noecho();
    curs_set(0);
    timeout(TIME_DELAY);
    keypad(stdscr, TRUE);

    /* initialize game field */
    for(int i = 0; i < FIELD_Y; i++)
    {
        for(int j = 0; j < FIELD_X; j++)
        {
            field[i][j] = CHAR_EMPTY;
        }
    }

    /* create gamefield window */
    WINDOW* window = newwin(FIELD_Y + 2, FIELD_X + 2, 0, 0);
    if(!window)
    {
        fprintf(stderr, "Couldn't initialize curses window!\n");
        exit(EXIT_FAILURE);
    }

    return window;
}

void shutdown(WINDOW* window)
{
    delwin(window);
    endwin();
}

