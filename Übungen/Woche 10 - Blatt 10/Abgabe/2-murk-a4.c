#include <stdio.h>
#include <stdlib.h>
#define MAX 5

int enter_number(int input)
{
    printf("enter a number: ");
    scanf("%d", &input);
    return input;
}

int print_number(int input)
{
    printf("number = %d\n", input);
    return input;
}

int square_number(int input)
{
    printf("calculate %d * %d\n", input, input);
    return input * input;
}

int main(void)
{
    int input = 0;
    int (*pipeline[MAX])(int input) = {
        enter_number,
        print_number,
        square_number,
        print_number,
        NULL};

    for (int i = 0; i < MAX; i++)
    {
        if(*pipeline[i] != NULL) {
            input = pipeline[i](input);
            printf("---------------------\n");
        } else {
            printf("\npipeline finished.\n");
        }
    }

    return 0;
}