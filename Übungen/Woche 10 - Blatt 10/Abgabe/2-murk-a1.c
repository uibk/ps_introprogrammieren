#include <stdio.h>
#include <stdlib.h>

void mess(int *n)
{
    printf("%d\n", *n++);
    printf("%d\n", *++n);
    printf("%d\n", -2 [n]);
    printf("%d\n", (-2)[n]);
    printf("%d\n", n[0]);
}

int main(void)
{
    int myValues[] = {9, 0, 8, 1, 7, 2, 6, 3, 5, 4};
    int a = myValues[3];
    int *p1;
    p1 = &myValues[0];
    int b = *p1;
    int c = (*p1 + 3) - *(p1 + 3);
    p1 = myValues;
    int d = *(p1 + 5);
    p1 = &myValues[4];
    printf("p1:   %d   \n", *p1);
    int e = *(p1 - 1);
    myValues[2] = 12345;
    int f = *(p1 - 2);

    printf("a:   %d   \n", a);
    printf("b:   %d   \n", b);
    printf("c:   %d   \n", c);
    printf("d:   %d   \n", d);
    printf("e:   %d   \n", e);
    printf("f:   %d   \n", f);

    mess(&myValues[3]);
    return 0;
}