#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int firstVariable = 50;
	int secondVariable = 100;
	int y;
	
	printf("Before swapping:\n");
	printf("firstVariable: %d \n", firstVariable);
	printf("secondVariable: %d \n", secondVariable);
	
	y = firstVariable;
	firstVariable = secondVariable;
	secondVariable = y;
	
	printf("After swapping:\n");
	printf("firstVariable: %d \n", firstVariable);
	printf("secondVariable: %d \n", secondVariable);

	return EXIT_SUCCESS;
}