#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int n = 5;
	
	if((n % 13) == 0) {
		printf("Der gegebene Wert ist durch dreizehn teilbar\n");
	} else {
		printf("Der gegebene Wert ist nicht durch dreizehn teilbar\n");
	}

	return EXIT_SUCCESS;
}