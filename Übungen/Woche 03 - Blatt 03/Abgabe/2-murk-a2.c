#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int n = 50;
	
	while(n >= 1) {
	
		printf("%d ", n);
		
		n = n - 3;
	}
	return EXIT_SUCCESS;
}