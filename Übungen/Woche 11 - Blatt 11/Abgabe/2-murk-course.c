#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef union points {
    float ects;
    unsigned int sst;
} Points;

typedef struct course
{
    unsigned int courseNr;
    char *title;
    char *type;
    Points points;
} Course;

int check_courseNr(int courseNr);

Course *create_course(int courseNr, char *title, char *type, float ects, unsigned int sst)
{
    if (ects != 0 && sst != 0)
    {
        Course course = {courseNr, title, type, {ects}};
        Course *coursePtr = &course;
        return coursePtr;
    }
    else if (ects == 0)
    {
        Course course = {courseNr, title, type, {sst}};
        Course *coursePtr = &course;
        return coursePtr;
    }
    return NULL;
}

Course *scan_course()
{
    int courseNr, sst;
    char *title = malloc(254);
    char *type = malloc(2);
    float ects;

    printf("Type in your values: [Coursenumber, Title, Type, ECTS, SSt]:\n");
    scanf("%d %s %s %f %d", &courseNr, &title[0], &type[0], &ects, &sst);
    if (check_courseNr(courseNr))
    {
        Course *course = create_course(courseNr, title, type, ects, sst);
        return course;
    }
    else
    {
        exit(0);
    }
}

int check_courseNr(int courseNr)
{
    int size = floor(log10(abs(courseNr))) + 1;
    if (size < 8)
    {
        printf("Size of Coursenumber should be more or equal to 8!\n");
        return 0;
    }
    else
    {
        return 1;
    }
}

void print_course(Course course)
{
    printf("-----------------------------------\n");
    printf("Kursnummer: %d\n", course.courseNr);
    printf("Titel: %s\n", course.title);
    printf("Typ: %s\n", course.type);
    if (course.points.ects == 0)
    {
        printf("SSt: %d\n", course.points.sst);
    }
    else
    {
        printf("ECTS: %.2f\n", course.points.ects);
    }
    printf("-----------------------------------\n");
}

int main(int argc, char const *argv[])
{
    Course *course = scan_course();
    print_course(*course);
    return 0;
}
