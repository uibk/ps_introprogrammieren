#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 10

typedef struct birthdate
{
    unsigned int day : 5;
    unsigned int month : 4;
    unsigned int year : 11;
} Birthdate;

typedef struct student
{
    unsigned int matrikelNr;
    char *firstName;
    char *lastName;
    Birthdate birthday;
} Student;

Student *create_student_list();
void print_student_list();
Student *search_in_student_list();
void insert_in_student_list();
int remove_in_student_list();
void sort_student_list(Student *students, int (*function)(Student, Student));
int sort_by_number(Student student1, Student student2);
int sort_by_firstname(Student student1, Student student2);
int sort_by_lastname(Student student1, Student student2);
int sort_by_birthday(Student student1, Student student2);

Student *create_student_list()
{
    Student *students = malloc(sizeof(Student) * SIZE);
    for (int i = 0; i < SIZE; i++)
    {
        students[i].firstName = "x";
        students[i].lastName = "x";
        students[i].birthday.year = 2019;
        students[i].birthday.day = 1;
        students[i].birthday.month = 1;
    }
    return students;
}

void print_student_list(const Student *students)
{
    for (int i = 0; i < SIZE; i++)
    {
        printf("-----------------------------------\n\n");
        printf("Student: %d\n", i + 1);
        printf("Matrikelnummer: %d\n", students[i].matrikelNr);
        printf("Vorname: %s\n", students[i].firstName);
        printf("Nachname: %s\n", students[i].lastName);
        printf("Geburtsdatum: %d.%d.%d\n", students[i].birthday.day, students[i].birthday.month, students[i].birthday.year);
        printf("-----------------------------------\n");
    }
}

Student *search_in_student_list(const Student *students, int matrikelNr)
{
    for (int i = 0; i < SIZE; i++)
    {
        if (students[i].matrikelNr == matrikelNr)
        {
            Student student = students[i];
            Student *studentPtr = &student;
            return studentPtr;
        }
    }
    return NULL;
}

void insert_in_student_list(Student *students, Student student)
{
    for (int i = 0; i < SIZE; i++)
    {
        if (students[i].matrikelNr == 0)
        {
            students[i] = student;
            break;
        }
    }
}

int remove_in_student_list(Student *students, int matrikelNr)
{
    for (int i = 0; i < SIZE; i++)
    {
        if (students[i].matrikelNr == matrikelNr)
        {
            students[i] = students[i + 1];
            return 1;
        }
    }
    return 0;
}

int sort_by_number(Student student1, Student student2)
{
    if (student1.matrikelNr < student2.matrikelNr)
    {
        return -1;
    }
    if (student1.matrikelNr > student2.matrikelNr)
    {
        return 1;
    }
    if (student1.matrikelNr == student2.matrikelNr)
    {
        return 0;
    }

    exit(0);
}
int sort_by_firstname(Student student1, Student student2)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (student1.firstName[j] > student2.firstName[j])
            {
                return 1;
            }
            if (student1.firstName[j] < student2.firstName[j])
            {
                return -1;
            }
            if (student1.firstName[j] == student2.firstName[j])
            {
                return 0;
            }
        }
    }
    exit(0);
}
int sort_by_lastname(Student student1, Student student2)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (student1.lastName[j] > student2.lastName[j])
            {
                return 1;
            }
            if (student1.lastName[j] < student2.lastName[j])
            {
                return -1;
            }
            if (student1.lastName[j] == student2.lastName[j])
            {
                return 0;
            }
        }
    }
    exit(0);
}
int sort_by_birthday(Student student1, Student student2)
{
    int student1BirthYear = student1.birthday.year;
    int student2BirthYear = student2.birthday.year;

    if (student1BirthYear > student2BirthYear)
    {
        return 1;
    }
    if (student1BirthYear < student2BirthYear)
    {
        return -1;
    }
    if (student1BirthYear == student2BirthYear)
    {
        if (student1.birthday.month > student2.birthday.month)
        {
            return 1;
        }
        if (student1.birthday.month < student2.birthday.month)
        {
            return -1;
        }
        if (student1.birthday.month == student2.birthday.month)
        {
            if (student1.birthday.day > student2.birthday.day)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
    return 0;
}

void sort_student_list(Student students[SIZE], int (*f)(Student, Student))
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < i; j++)
        {
            int result = (*f)(students[j], students[j + 1]);

            if (result == 1)
            {
                Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }
}

int main(int argc, char const *argv[])
{
    Student *studentList = create_student_list();
    Student student = {10, "Andreas", "Feilchen", {24, 3, 1996}};
    Student student2 = {2, "Thomas", "Pinggera", {11, 11, 1989}};
    Student student3 = {17, "Helmut", "Untermoser", {12, 9, 1954}};
    Student student4 = {1, "Anton", "Broger", {12, 9, 1975}};
    insert_in_student_list(studentList, student);
    insert_in_student_list(studentList, student2);
    insert_in_student_list(studentList, student3);
    insert_in_student_list(studentList, student4);
    // print_student_list(studentList);
    // print_student_list(studentList);
    sort_student_list(studentList, sort_by_birthday);
    print_student_list(studentList);
    remove_in_student_list(studentList, 10);
    print_student_list(studentList);
    return 0;
}
