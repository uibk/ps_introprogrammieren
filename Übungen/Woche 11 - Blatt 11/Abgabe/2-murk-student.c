#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 20

typedef struct birthdate
{
    unsigned int day : 5;
    unsigned int month : 4;
    unsigned int year : 11;
} Birthdate;

typedef struct student
{
    unsigned int matrikelNr;
    char *firstName;
    char *lastName;
    Birthdate birthday;
} Student;

int check_student(Student student);
int check_date(int day, int month, int year);
void print_student(Student student);

int check_student(const Student student)
{
    if (student.matrikelNr > 0 && strlen(student.firstName) >= 1 && strlen(student.lastName) >= 1 && check_date(student.birthday.day, student.birthday.month, student.birthday.year))
    {
        return 1;
    }
    return 0;
}

int check_date(int day, int month, int year)
{
    if (day > 0 && day <= 31 && month > 0 && month <= 12 && year > 0 && year >= 1950)
    {
        printf("\ncorrect information!\n");
        return 1;
    }
    else
    {
        printf("\nincorrect information!\n");
    }
    return 0;
}

Student create_student(int matrikelNr, char *firstName, char *lastName, int day, int month, int year)
{
    Student student = {matrikelNr, firstName, lastName, {day, month, year}};
    return student;
}

Student scan_student()
{
    int matrikelNr, day, month, year;
    char *firstName = malloc(256);
    char *lastName = malloc(256);
    printf("Type in your values: [Number, Firstname, Lastname, Day, Month, Year]:\n");
    scanf("%d %s %s %d %d %d", &matrikelNr, &firstName[0], &lastName[0], &day, &month, &year);
    Student student = create_student(matrikelNr, firstName, lastName, day, month, year);
    if (check_student(student))
    {
        print_student(student);
        return student;
    }
    Student student1 = {0, 0, 0, {0, 0, 0}};
    return student1;
}

void print_student(const Student student)
{
    printf("Matrikelnummer: %d\n", student.matrikelNr);
    printf("Vorname: %s\n", student.firstName);
    printf("Nachname: %s\n", student.lastName);
    printf("Geburtsdatum: %d.%d.%d\n", student.birthday.day, student.birthday.month, student.birthday.year);
}

int main(int argc, char const *argv[])
{
    scan_student();
    return 0;
}
