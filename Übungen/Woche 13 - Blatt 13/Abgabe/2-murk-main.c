#include "2-murk-r1e.h"
#include "2-murk-file_io.h"

void commandoParameter(char **parameter, char *inputFile, char *outputFile)
{
    if (inputFile != NULL && outputFile != NULL)
    {
            switch (parameter[1][1])
            {
            case 'e':
                encodeToOutput(inputFile, outputFile);
                break;
            case 'd':
                decodeToOutput(inputFile, outputFile);
                break;
            default:
                break;
            }
        }
    else
    {
        printf("One of the provided files is not valid!\n");
        exit(0);
    }
}

int main(int argc, char const **argv)
{
    commandoParameter(argv, argv[2], argv[3]);
    return 0;
}
