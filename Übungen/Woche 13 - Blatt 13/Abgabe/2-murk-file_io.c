#include "2-murk-file_io.h"

FILE *openFile(char const *fileName)
{
    FILE *file = fopen(fileName, "r");
    return file;
}

content_t *read_content(char const *fileName)
{
    FILE *file = openFile(fileName, "r");
    
    if (file != NULL)
    {
        content_t *content = allocMemory(file);

        for (int i = 0; i < content->length; i++)
        {
            content->lines[i] = malloc(256 * sizeof(char));
            fgets(content->lines[i], 256, file);
        }
        fclose(file);
        return content;
    }
    else
    {
        printf("Cannot open file!\n");
        exit(0);
    }
    return NULL;
}

FILE *openFile(char *fileName, char* mode)
{
    FILE *file = fopen(fileName, mode);
    return file;
}

void write_content(char *fileName, content_t *content)
{
    FILE *file = openFile(fileName, "w");

    if (file != NULL)
    {
        for (int i = 0; i < content->length; i++)
        {
            fprintf(file, "%s\n", content->lines[i]);
        }
    }
    fclose(file);
}

int count_lines(FILE *file)
{
    char current_char;
    int number_lines = 0;

    while ((current_char = getc(file)) != EOF)
    {
        if (current_char == '\n')
        {
            number_lines++;
        }
    }
    fseek(file, 0, SEEK_SET);
    return number_lines;
}

content_t *allocMemory(FILE *file)
{
    content_t *content = malloc(sizeof(content_t));
    content->length = count_lines(file);
    content->lines = malloc(content->length * sizeof(char *));
    return content;
}