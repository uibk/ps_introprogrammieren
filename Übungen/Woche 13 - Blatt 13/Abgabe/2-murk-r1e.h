#ifndef R1E_H_
#define R1E_H_
#include <string.h>

char *r1e_encode(char *line);

char *r1e_decode(char *line);

void encodeToOutput(char const *inputFile, char const *outputFile);

void decodeToOutput(char const *inputFile, char const *outputFile);

#endif
