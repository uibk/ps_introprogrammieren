#include "2-murk-r1e.h"
#include "2-murk-file_io.h"

char *r1e_encode(char *line)
{
    char current_char;
    int charCounter = 1;
    int i = 0;
    int i2 = 0;
    char *result = malloc(1000 * sizeof(char));

    while (line[i] != '\n')
    {
        current_char = line[i];
        if (current_char == line[i + 1])
        {
            charCounter++;
        }
        else
        {
            sprintf(&result[i2], "%c%d", current_char, charCounter);

            if (charCounter >= 10)
            {
                i2 += 3;
            }
            else if (charCounter >= 100)
            {
                i2 += 4;
            }
            else
            {
                i2 += 2;
            }
            charCounter = 1;
        }
        i++;
    }
    return result;
}

char *r1e_decode(char *line)
{
    char current_char;
    int charCounter = 0;
    int i = 0;
    char *result = malloc(1000 * sizeof(char));
    int temp = 0;
    int index = 0;
    while (line[i] != '\0')
    {
        if (line[i] != '\n')
        {
            current_char = line[i];
        }

        sscanf(&line[i + 1], "%d", &charCounter);
        
        temp = charCounter + index;

        for (; index < temp; index++)
        {
            sprintf(&result[index], "%c", current_char);
        }

        if (charCounter >= 10)
        {
            i += 3;
        }
        else if (charCounter >= 100)
        {
            i += 4;
        }
        else
        {
            i += 2;
        }
    }
    return result;
}

void freeContent(content_t *content)
{
    for (int i = 0; i < content->length; i++)
    {
        free(content->lines[i]);
        content->lines[i] = NULL;
        free(content->lines);
        content->lines = NULL;
        free(content);
        content = NULL;
    }
}

void freeMemory(content_t *originalContent, content_t *outputContent, FILE *fileStream)
{
    freeContent(originalContent);
    freeContent(outputContent);
    free(fileStream);
}

void encodeToOutput(char const *inputFile, char const *outputFile)
{
    content_t *originalContent = read_content(inputFile);

    FILE *newFileStream = openFile(inputFile);

    content_t *encodedContent = allocMemory(newFileStream);

    for (int i = 0; i < encodedContent->length; i++)
    {
        encodedContent->lines[i] = r1e_encode(originalContent->lines[i]);
    }
    write_content(outputFile, encodedContent);
    freeMemory(originalContent, encodedContent, newFileStream);
}

void decodeToOutput(char const *inputFile, char const *outputFile)
{
    content_t *originalContent = read_content(inputFile);

    FILE *newFileStream = openFile(inputFile);

    content_t *decodedContent = allocMemory(newFileStream);

    for (int i = 0; i < decodedContent->length; i++)
    {
        decodedContent->lines[i] = r1e_decode(originalContent->lines[i]);
    }

    write_content(outputFile, decodedContent);
    freeMemory(originalContent, decodedContent, newFileStream);
}
