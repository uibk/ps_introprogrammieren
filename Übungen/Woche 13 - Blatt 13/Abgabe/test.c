#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int main(void) {

    int array[4] = {0};
    printf("%d", strlen(&array));

}

void swap_ptr(int **a, int **b) {

    int **temp = *b;
    *b = *a;
    *a = *b;
}

int addInt(int n, int m) {
    return n + m;
}

int(*functionPtr)(int, int);

functionPtr = &addInt;

int addxToY(int(*functionPtr(2,3)));

int (functionFactory(int n)) (int, int) {
printf("Got parameter %d", n);
int(*functionPtr(int,int)) = &addInt;
return functionPtr;
}