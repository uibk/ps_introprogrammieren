#ifndef FILE_IO_H_
#define FILE_IO_H_
#define FILE_NAME "/home/andreas/Desktop/ps_introprogrammieren/Übungen/Woche 13 - Blatt 13/Abgabe/ascii_art.txt"
#include <stdio.h>
#include <stdlib.h>

typedef struct _content
{
    int length;
    char **lines;
} content_t;

content_t *read_content(char *filename);

void write_content(char const *filename, content_t *content);

content_t *allocMemory(FILE *file);

int count_lines(FILE *file);

void freeContent(content_t *content);

FILE *openFile(char *fileName, char *mode);

#endif
