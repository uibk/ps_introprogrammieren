#!/bin/bash

# Build file_io module
gcc -Wall -Werror -g -c 2-murk-file_io.c

# Build encode/decode module
gcc -Wall -Werror -g -c 2-murk-r1e.c

# Build module containting main
gcc -Wall -Werror -g -c 2-murk-main.c

# Link them together
gcc -Wall -Werror -g -o 2-murk-main 2-murk-file_io.o 2-murk-r1e.o 2-murk-main.o