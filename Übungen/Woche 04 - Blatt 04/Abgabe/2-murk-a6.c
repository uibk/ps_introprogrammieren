#include <stdio.h>

int main(void) {
 
    float input = 1;
    int counter = 0;
    double sum = 0.00;
    double average = 0.00;

    while(input != 0) {
        scanf("%f", &input);
        sum = sum + input;
        average = (sum / counter);
        if (input != 0) counter++;
    }
    printf("Amount: %d, Sum: %.2f, Average: %.2f\n", counter, sum, average);
    
    return 0;
}
