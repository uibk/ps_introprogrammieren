#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    const double GRAVITATION = 6.673e-11;
    float mass1 = 3.33;
    float mass2 = 3.05;
    float abstand = 30;
    char c = 'G';
    int zero = 0;
    double ergebnis;

    ergebnis = (GRAVITATION * (mass1 * mass2)) / (abstand * abstand);

    printf("Masse 1 = %.4f kg, Masse 2 = %.4f kg, Abstand = %.2f m\n", mass1, mass2, abstand);
    printf("Exponential: %e\n", ergebnis);
    printf("Dezimal: %.25f\n", ergebnis);
    printf("Variable c: %x\n", c);
    printf("Zero: %d\n", zero);
}
