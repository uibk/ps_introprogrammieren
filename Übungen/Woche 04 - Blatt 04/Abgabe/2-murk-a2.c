#include <stdio.h>

int main(void)
{
	float start = 3;
	int stop = 4;
	float step = 0.3;

	if (stop < start || step <= 0)
	{
		printf("%s\n", "Invalid assignment of values");
		return 0;
	}
	while (start < stop)
	{
		printf("%.2f    ", (start));
		start += step;
	}
		printf("\n");
	return 0;
}
