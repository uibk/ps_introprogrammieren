#include <stdio.h>

int main(void)
{
    int first = 0;
    int second = 1;
    int n = 10;
    int sum = 1;

    printf("%d %d", first, second);

    while (sum <= n) {
        sum = first + second;
        first = second;
        second = sum;
        if (sum <= n) printf(" %d", sum);
    }
    printf("\n");
    return 0;
}
