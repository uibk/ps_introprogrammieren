#include <stdio.h>
#include <stdbool.h>

 enum status {
        submitted = 0,
        underway,
        delivered
    };

int main(void) {
   
    enum status currentStatus = delivered;
    bool priority = false;

    switch (currentStatus) {
    case 0:
        printf("%s\n", "The package has been submitted!");
        break;
    case 1:
        switch (priority) {
            case 1: printf("%s\n", "The package is almost there!");
            break;
            case 0: printf("%s\n", "The package is underway!");
            break;
        }
        break;
    case 2:
        printf("%s\n", "The package has been delivered!");
        break;
    default:
        break;
    }
    return 0;
}
