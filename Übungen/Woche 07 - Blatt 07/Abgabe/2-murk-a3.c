#include <stdio.h>
#include <time.h>
#include "daysofmonth.h"
#include "weekdays.h"
#include "months.h"

enum months
{
    january = 1,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    october,
    november,
    december
};

enum daysInMonth
{
    normal = 31,
    unnormal = 30,
    febDays = 28,
};

int countDaysOfMonth(int currentMonth, int dayCounter)
{
    switch (currentMonth)
    {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        dayCounter = normal;
        break;
    case 2:
        dayCounter = febDays;
        break;
    case 4:
    case 6:
    case 9:
    case 11:
        dayCounter = unnormal;
    }

    return dayCounter;
}

int printWeekDays(int counter, char *weekDays[], int dayCounter)
{
    for (int i = 1; i <= dayCounter; i++)
    {
        if (counter < 7)
        {
            if (i < 10)
            {
                printf("| %s 0%d ", weekDays[counter], i);
            }
            else
            {
                printf("| %s %d ", weekDays[counter], i);
            }
        }
        else
        {
            printf("\n");
            
            counter = 0;
            if (i < 10)
            {
                printf("| %s 0%d ", weekDays[counter], i);
            }
            else
            {
                printf("| %s %d ", weekDays[counter], i);
            }
        }
        counter++;
    }
    return counter;
}

void countMonths(int currentMonth, int dayCounter, char *weekDays[], int counter)
{
    for (; currentMonth <= 12; currentMonth++)
    {
        countDaysOfMonth(currentMonth, dayCounter);

        printf("Month %d (%d days)\n", currentMonth, dayCounter);

        counter = printWeekDays(counter, weekDays, dayCounter);

        printf("\n\n");
    }
}

int main(void)
{
    enum months currentMonth = january;
    enum daysInMonth dayCounter = normal;
    char *weekDays[7] = {"MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"};
    int counter = 0;

    countMonths(currentMonth, dayCounter, weekDays, counter);

    return 0;
}