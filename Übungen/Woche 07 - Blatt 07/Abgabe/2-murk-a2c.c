#include <stdio.h>
#include <ctype.h>

int main(void) {

    // while(1) {
    //     char letter;
    //     scanf("%c", &letter);

    //     int c;
    //     while((c = getchar()) != '\n' && c != EOF) {}

    //     if(!isalpha(letter)) break;
    // }

    for(; 1 ;) {
        char letter;
        scanf("%c", &letter);

        int c;
        while((c = getchar()) != '\n' && c != EOF) {}

        if(!((letter >= 'A' && letter <= 'Z') || (letter >= 'a' && letter <= 'z'))) break;
    
    }

    return 0;
}