#include <stdio.h>

int main(void) {

    // int i = 1;
    // for(; i++ < 10;) {
    //     switch(i) {
    //         case 2:
    //         printf("start\n");
    //         break;
    //         case 10:
    //         printf("end\n");
    //         break;
    //         case 6: printf("half ");
    //         default: printf("%d\n", i);
    //     }
    // }

    int i = 1;
    while(++i < 10) {
        if(i == 2) {
              printf("start\n");
        } else if (i == 6) {
            printf("half %d\n", i);
        } else if (i == 10) {
              printf("end\n");
        } else {
        printf("%d\n", i);
        }
    }

    return 0;
}