#include <stdio.h>

int main(void) {
    float f = 8.5;
    float f2 = 0.0;
    double d = 36.112;
    int i = -10;
    char c = 'a';
    short s = 5;
    unsigned int ui = 88;

    /* Code */

    d = i + f;
    i = (ui - 150) * 50;
    f2 = d * 55;
    c += s;

    printf("%f\n", f);
    printf("%f\n", f2);
    printf("%f\n", d);
    printf("%d\n", i);
    printf("%c\n", c);
    printf("%c\n", s);

    return 0;
}