/*
	RACE!
	
	(1) Kompiliere das Spiel und führe es aus.
	
	(2) Was macht rand(), srand() und time()? Lese dir je den ersten Absatz der DESCRIPTION in den dazugehörigen manpages durch.
	rand() gibt Zufallszahl aus. % bestimmt Range.
	srand() 
	time() gibt die aktuelle Zeit seit 1970 (UNIX-TIMESTAMP) in Millisekunden aus.
	(3) Räume den Code auf: 
		- versuche den Code überblicksmäßig zu verstehen
		- verwende all dein Wissen dazu den Code verständlicher zu machen
		- formatiere den Code
			- ein Befehl pro Zeile
			- eine Deklaration pro Zeile
			- rücke den Code richtig ein
			- ...
		- schreibe Codestellen um;  
			- verwende passende Schleifenarten, Variablentypen, ... 
			- verwende Konstanten, Enums, ...
			- keine globalen Variablen (außerhalb von main)
			- ...
		- entferne unnötigen Code
		- kommentiere Stellen
		- ...

	(4) Die Straße kann links aus dem Bildschirm verschwinden. Behebe dieses Problem.
	
	(5) Wenn man bspw. immer 'd' eingibt, kann man über den Straßenrand fahren. Finde den Fehler und behebe ihn.
	
	(6) Lass den Spieler sein Fahrzeugzeichen eingeben.
	
	(7) Wenn der Spieler nach einer Bewegung direkt neben der linken oder rechten Straßenbegrenzung steht, soll er einen Punkt zusätzlich bekommen. Gib zusätzlich "ACHTUNG!" aus.  
	
	(8) Die Straßenbreite soll sich im Laufe des Spiels ändern. Zu Beginn soll die Straße 8 Zeichen breit sein. Nach jeder vierten Bewegung soll die Straßenbreite eins schmäler werden, bis sie nur noch 3 Zeichen breit ist.
	
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


enum direction {
	NONE = 0,
	RIGHT,
	LEFT
};

int main(void){
	unsigned car = 'V';
	unsigned player=10;
	unsigned left=7;
	unsigned right=13;
	unsigned i=0;
	unsigned points=0;

	unsigned int now = (unsigned int)time(NULL);
	
	// initialisiere den Zufallsgenerator mit der aktuellen Zeit
	srand(now);

	printf("           \n");
	printf("   _____     \n");
	printf("  /_..._\\    \n");
	printf(" (0[###]0)  RACE! \n");
	printf("  `'   `'    \n");
	printf("           \n");
	printf("    [a] left \n");
	printf("    [s] straight ahead \n");
	printf("    [d] right\n");
	printf("\n");
	
	while(1==1){
		char c;
		scanf(" %c",&c);
		int direction; 
		int random_number = rand() % 100; // Zahl zwischen 0 und 99

		if(random_number <= 49) {
			direction = RIGHT;
		} else {
			direction = LEFT;		
		} if(c == 'a') {
			player--;	 
		} if(c == 'd')	{				
			player++;
		}			
		
		switch(direction)
		{
			// direction = RIGHT
			case 1: 
				left +=1; 
				right++; 
				break;
			// direction = LEFT
			case 2: 
				if(left >= 0) left -=1;
				if(right >=0) right--;
				break;
		}
		
		while(i < 40 && i >= 0) {
		if(i == left) { // i eq 7
			printf(")");
		} else if (i == right) { // i eq 13
			printf("(");
		} else if(i == player) { // i eq 10
			// printf("I: %d\n", i);
			printf("%c\n", car);
		} else {
			printf(" ");
		}  
		i++;
		if(i > 39) {
			i = 0;
			break;
		}
	}
		printf("\n");
		i=0;
		
		if (player == left || player == right) {
			break;
		} else {
			points+=1;
		}
	}
	
	printf("GAME OVER.          \nPOINTS: %d\n", points);	
}
